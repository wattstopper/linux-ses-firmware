# Segger Embedded Studio Docker Image

Basic image to allos Segger Embedded Studio (SES) builds from Docker.

This is used for continuous integration (CI) of SES projects.

The SES tools are installed into the image under /ses. The build command is `/ses/bin/emBuild`.


## Contents

* [Features](#features)
* [Installation](#installation)
* [Usage](#usage)
* [Configuration](#configuration)
* [Examples](#examples)

## Features
* [Seeger Embedded Studio (SES)](https://www.segger.com/downloads/embedded-studio) - 
* [nRF Command-Line tools](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Command-Line-Tools/Download#infotabs) - 
* [nRF SDK v15.x.x](http://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v15.x.x/) - 

## Usage

[legrandbcs/linux-ses-firmware](https://cloud.docker.com/repository/docker/legrandbcs/linux-ses-firmware)

To build a project startup the docker image while linking the source driectory to the path `/src`

## Examples

`docker run -it legrandbcs/linux-ses-firmware /ses/bin/emBuild`

Prints help information...

`docker run -it legrandbcs/linux-ses-firmware /ses/bin/emBuild -v <host source file path>:/src -config 'Release' /src/test-project.emProject`

Performs a build...