FROM ubuntu:bionic

## This is derived from the original https://github.com/josschne/ses/blob/master/config_3.50_sdk_15.0.0/Dockerfile
##

RUN apt-get update && \
	apt-get install -y libx11-6 libfreetype6 libxrender1 libfontconfig1 libxext6 xvfb curl unzip python-pip git zip python3 clang-format && \
	pip install nrfutil

RUN Xvfb :1 -screen 0 1024x768x16 &

#  https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Command-Line-Tools/Download#infotabs
RUN curl -X POST -F "fileid=F6A269554D834518A432AACA0FD36FE5" https://www.nordicsemi.com/api/sitecore/Products/DownloadPlatform -o nrftools.tar && \
    mkdir /nrf_tmp && \
	tar -xvf nrftools.tar -C /nrf_tmp && \
    tar -xzf /nrf_tmp/nRF-Command*tar.gz && \
    rm -rf /nrf_tmp && \
	rm nrftools.tar
ENV PATH="/mergehex:/nrfjprog:$PATH"

# https://www.segger.com/downloads/embedded-studio
RUN curl https://www.segger.com/downloads/embedded-studio/Setup_EmbeddedStudio_ARM_v350_linux_x64.tar.gz -o ses.tar.gz && \
	tar -zxvf ses.tar.gz && \
	DISPLAY=:1 $(find arm_segger_* -name "install_segger*") --copy-files-to /ses && \
	rm ses.tar.gz && \
	rm -rf arm_segger_embedded_studio_*

# http://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v15.x.x/
RUN curl http://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v15.x.x/nRF5_SDK_15.3.0_59ac345.zip -o nRF5_SDK_15.3.0_59ac345.zip && unzip nRF5_SDK_15.3.0_59ac345.zip && rm nRF5_SDK_15.3.0_59ac345.zip

CMD ["/ses/bin/emBuild"]

LABEL Name=linux-ses-firmare Version=0.0.2
